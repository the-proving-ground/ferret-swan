require "uri"
require "net/http"

class Client
  class CircuitBreakerError < StandardError; end

  attr_accessor :debug, :error_threshold, :time_window
  attr_reader :errors

  def initialize(time_window: nil, error_threshold: 3, debug: false)
    self.error_threshold = error_threshold
    self.time_window = time_window
    puts "initialized client with #{time_window} second window of #{error_threshold} errors" if debug
    @errors = []
    @debug = debug
  end

  def request(url)
    purge_old_errors
    puts "requesting #{url}" if debug
    response = get url
    response.body if response.is_a?(Net::HTTPSuccess)
  rescue StandardError => e
    puts "error: #{e}" if debug
    @errors << Time.now
    raise CircuitBreakerError if @errors.count > error_threshold
  end

  private

  def get(url)
    uri = url.is_a?(URI) ? url : URI(url)
    Net::HTTP.get_response uri
  end

  def purge_old_errors
    if debug
      count = @errors.select { |i| i < (Time.now - time_window) }.size
      puts "purging #{count} errors"
    end
    @errors.reject! { |i| i < (Time.now - time_window) }
  end
end
