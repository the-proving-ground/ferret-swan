# Ferret Swan

This repo includes networking assignment code.

## HTTP Client

Initialize a client and request a URL to return the response body:

    require "ferret-swan"
    client = Client.new
    client.request url

Test the client with rake and minitest:

    rake test

## Using cURL

An unsubstantiated travelogue of a curl request...

    curl example.com

When I query an address using cURL on my MacBook, the following occurs:

+ the shell checks $PATH for binary
+ the terminal (running bash in my case) executes a kernel command to load the curl program into memory
+ the kernel generates a pid for the curl process
+ curl is loaded with a parent of the shell process, and enters a running state
+ curl parses the command args (in my case only the url)
+ curl loads my ~/.curlrc config
+ curl checks/guesses protocol (http)
+ curl queries the default network interface
+ curl check hosts file
+ curl opens a tcp socket
+ curl cycles states during dns queries...
+ curl performs system calls to the kernel for networking, disk cache, etc...
+ dns query to my local network (deco mesh)
+ dns query to isp (ziggo)
+ dns query to name server
+ curl obtains an ip for the site
+ curl packages tcp/ip packets and sends a request to establish a connection with the site
+ curl cycles states during tcp handshake...
+ the site completes the http handshake and establishes a connection
+ curl creates an http request including default headers for content, agent, etc.
+ curl packages tcp/ip packets for the request...
+ the site responds with a 301 redirect status
+ curl extracts the location and because my .curlrc is configured to do so, follows the redirect automatically
+ curl checks/guesses protocol (now https)
+ curl repeats the connection process and http handshake
+ curl determines which cipher suite from the site response
+ curl requests the ssl certificate chain (4 certs, 3 CAs)
+ curl performs calls to (in my case) openssl to authenticate the certificate
+ the site completes the https handshake by generating a session key
+ curl switches to encrypted communication
+ curl encrypts the request contents (in this case only headers, etc. no body) and packages tcp/ip packets for the request...
+ curl receives response packets from the site which it buffers
+ curl completes the transfer and sends the buffered response body to stdout
+ in my case output is my terminal interface, and i see the html printed to the screen
+ curl sends a successful exit code to the kernel
+ the kernel terminates the curl process and releases associated resource (e.g. memory)
+ the kernel notifies my terminal (the parent of the curl process)
+ the terminal returns to the interactive session awaiting input signals
