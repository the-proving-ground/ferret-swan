require "test_helper"

class ClientTest < TestCase
  def test_circuit_breaker_not_tripped
    error_threshold = 2
    time_window = 0.1
    client = Client.new time_window: time_window, error_threshold: error_threshold, debug: true
    mock_http_error client do
      (error_threshold + 1).times do
        client.request "https://example.com"
        sleep time_window / error_threshold
      end
    end
    assert_equal error_threshold, client.errors.size
  end

  def test_circuit_breaker_tripped
    error_threshold = 3
    client = Client.new time_window: 2, error_threshold: error_threshold, debug: true
    mock_http_error client do
      (error_threshold + 1).times do
        client.request "https://example.com"
      end
    end
  rescue Client::CircuitBreakerError
    assert_equal error_threshold + 1, client.errors.size
  end

  private

  def mock_http_error(client, &block)
    client.stub :get, mock_exception(Client::CircuitBreakerError), &block
  end
end