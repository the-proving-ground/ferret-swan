$:.push File.expand_path('../../lib', __FILE__)

require_relative "../lib/client"

require "minitest/autorun"

class TestCase < Minitest::Test
  private

  def mock_exception(error)
    -> (*args) { raise error.new(*args) }
  end
end
